angular
    .module("Humeds", ["Controllers"])
    .config( function($routeProvider){
        $routeProvider
            .when('/', {
                templateUrl: "templates/dashboard.html",
                controller: "dashboard_controller"
            })
            .when('/patient_administration', {
                templateUrl: "templates/patient_administration_search.html",
                controller: "patient_administration_search"
            })
            .when('/patient_administration/view/:index',{
                templateUrl: "templates/patient_administration_card.html",
                controller: "patient_administration_card"
            })
            .when('/patient_administration/add', {
                templateUrl: "templates/patient_administration_add.html",
                controller: "patient_administration_add"
            })

    })
    .controller("main_controller",["$scope", function($scope){

    }]);