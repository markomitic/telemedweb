angular
    .module("Dashboard/Patient administration", [])
    .controller("dashboard_patient_administration", ['$scope', function($scope){
        var temp_data = new Date();
        $scope.id = "ng-generic-id-pa"+ temp_data.getMilliseconds();
        $scope.messages = [
            {
                from: "Test Sender",
                subject: "Test Subject"
            },
            {
                from: "Test Sender2",
                subject: "Test Subject2"
            }
        ];

    }]);