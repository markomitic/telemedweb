angular
    .module("Dashboard/Signal interpretation", [])
    .controller("dashboard_signal_interpretation", ['$scope', function($scope){
        var temp_data = new Date();
        $scope.id = "ng-generic-id-recents"+ temp_data.getMilliseconds();
        $scope.recordings = [
            {
                patient_pid: "2CRX1"
            },
            {
                patient_pid: "2CRX2"
            }
        ];

    }]);