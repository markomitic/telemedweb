angular
    .module("Patient administration/card", ['ui.bootstrap.accordion'])
    .controller("patient_administration_card", function($scope, $routeParams, $location, PatientService){
        $scope.inner_template = "templates/partial/patient administration card/basic_data.html";
        console.log($scope.$parent.test);
        var t = new Date();
        $scope.random_number = t.getMilliseconds();
        $scope.menu_items = [
            {
                title: "Basic data",
                template: "templates/partial/patient administration card/basic_data.html"
            },
            {
                title: "Recordings",
                template: "templates/partial/patient administration card/recordings.html"
            }
        ];

        $scope.changeContent = function(url,Ni){
            if (Ni.$dirty){
                Ni.$dirty = false;
                var ans = confirm("Do you want to save changes?");
                if (ans) {
                    $scope.commit();

                }
                else {
                    $scope.discard();
                }
            }
            $scope.inner_template = url;
        };

        $scope.index = PatientService.getPatientToView();
        PatientService.getPatient($scope.index).then(function(data){
            $scope.patient = data;
        });

        $scope.patient_backup = angular.copy($scope.patient);

        $scope.commit = function(){
            $scope.patient_backup = angular.copy($scope.patient);
        };
        $scope.discard = function(){
            $scope.patient = angular.copy($scope.patient_backup);
        };

        $scope.save_changes = function(Ni){
            $scope.commit();
            Ni.$dirty = false;

        };
        $scope.remove_patient = function(){
            var ans = confirm("Are you sure that you wanna delete this patient?");
            if (ans){
                patients.splice($scope.index, 1);
                $location.path("/patient_administration")
            }
            else {

            }
        };
        $scope.$on('$locationChangeStart', function(scope, next, current){
            if ($scope.Ni.$dirty){
                var ans = confirm("Are you sure that you want to leave this page? All changes will be discarded!");
                if (ans){
                    $scope.discard();
                }
                else {
                    scope.preventDefault();
                }
            }

        });
    });