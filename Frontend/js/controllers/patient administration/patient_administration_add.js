angular
    .module("Patient administration/add", ['ui.bootstrap.accordion'])
    .controller("patient_administration_add", function($scope, $http){
        var t = new Date();
        $scope.patient = {};
        $scope.random_number = t.getMilliseconds();
        $scope.patient = {
            "pid": "",
            "first_name": "",
            "last_name": "",
            "middle_name": "",
            "mothers_name": "",
            "fathers_name": "",
            "maiden_name": "",
            "birth_date": null,
            "birth_place": "",
            "weight": 0,
            "height": 0,
            "blood_type": "",
            "sex": "",
            "native_language": "",
            "nationality": "",
            "marital_status": "",
            "educational_status": "",
            "country_of_residence": "",
            "death_indicator": false,
            "death_date": null,
            "has_children": false
        };
        $scope.save_patient = function(){
            $http({
                method: "POST",
                url: "http://telemed.eu01.aws.af.cm/patients/",
                data: angular.toJson($scope.patient)

            })
                .success(function(){
                    console.log("Uspeh!")
                })
                .error(function(){
                    console.log("Neuspeh!")
                });
            console.log(angular.toJson($scope.patient));
        };
});