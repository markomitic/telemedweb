angular
    .module("Patient administration", ["Patient administration/search", "Patient administration/card", "Patient administration/add"])
    .controller('patient_administration', function($scope){

    })
    .service("PatientService", function($http){
        var id_patient = 0;
        return {
            getAllPatients: function(page_num){
                var promise = $http.get('http://telemed.eu01.aws.af.cm/patients/?page='+page_num)
                    .then(function(response){
                        return response.data;
                    });
                return promise;
            },
            getPatient: function(index){
                var promise = $http.get('http://telemed.eu01.aws.af.cm/patients/' + index + '/')
                    .then(function(response){
                        return response.data;
                    });
                return promise;

            },
            setPatientToView:  function(id){
                id_patient= id;
            },
            getPatientToView: function(){
                return id_patient;

            }

        }
    });
