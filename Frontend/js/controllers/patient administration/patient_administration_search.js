angular
    .module("Patient administration/search", ['ui.bootstrap.pagination'])
    .controller("patient_administration_search", function($scope, PatientService){
        var t = new Date();
        $scope.random_number = t.getMilliseconds();
        $scope.current_page = 1;
        PatientService.getAllPatients($scope.current_page).then(function(data){
            $scope.patients = data.results;
            $scope.hasNext = data.next;
            $scope.hasPrevious = data.previous;
        });
        $scope.previous = function(){
            if ($scope.hasPrevious != null){
                    $scope.current_page--;
                    PatientService.getAllPatients($scope.current_page).then(function(data){
                        $scope.patients = data.results;
                        $scope.hasNext = data.next;
                        $scope.hasPrevious = data.previous;
                    });
            }
        };
        $scope.next = function(){
            if ($scope.hasNext != null){
                $scope.current_page++;
                PatientService.getAllPatients($scope.current_page).then(function(data){
                    $scope.patients = data.results;
                    $scope.hasNext = data.next;
                    $scope.hasPrevious = data.previous;
                });
            }
        }
        $scope.setPatient = function(index){
            PatientService.setPatientToView(index);
            console.log (index);
        }


    });